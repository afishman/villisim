# 

declare -a vals=("1" "10")

source ./job.sh

# Sets a simulation parameter
JID=""

## Loop over each parameter
for val in "${vals[@]}"
do
    ## Update json
    # set name
    echo $val

    # Set param
    if [ -z "$JID" ]
        then
            JID=$(job ./set_param $basename $param $val)
        else
            JID=$(job --dependency=afterany:$JID "./set_param $basename $param $val")
    fi

    # Queue sim
    JID=$(job --dependency=afterany:$JID ./sim.sh)
    echo sim jid $JID

done
