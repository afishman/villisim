SIMROOT='/home/afishman/flow-modelling/afishman/sims/'
#declare -a VALS=("inphase_re_1" "inphase_re_10" "inphase_re_100" "inphase_re_10000")

declare -a VALS=(
    "inphase_re_1" "antiphase_re_1" 
    "inphase_re_10" "antiphase_re_10" 
    "inphase_re_100" "antiphase_re_100" 
    "inphase_re_1000" "antiphase_re_1000" 
    "inphase_re_2000" "antiphase_re_2000" 
    "inphase_re_4000" "antiphase_re_4000" 
    "inphase_re_6000" "antiphase_re_6000" 
    "inphase_re_8000" "antiphase_re_8000" 
    "inphase_re_10000" "antiphase_re_10000" )

#declare -a VALS=("antiphase_re_2000")

cd ./template

for i in "${VALS[@]}"
do
    MATDIR="${SIMROOT}mat/${i}/"
    POSTPROCESSINGDIR="${SIMROOT}plots/${i}/"

    sbatch ./ftleVid.sh $MATDIR $POSTPROCESSINGDIR 
done

