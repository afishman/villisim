#!/bin/bash
#
#SBATCH --job-name=ftlecombine
#SBATCH --output=ftlecombineout
#
#SBATCH --ntasks=1
#SBATCH --time=05:00:00
#SBATCH --mem-per-cpu=20000
#SBATCH --nodes=1


OUTDIR=$(readlink $1 -f)


cd /home/afishman/flow-modelling/afishman/streaklines/

module load matlab/2017b
srun sh mrun.sh combineFtleVid $OUTDIR

echo done
