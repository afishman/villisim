#!/bin/bash
#
#SBATCH --job-name=ftlevid
#SBATCH --output=ftlevidout
#
#SBATCH --ntasks=1
#SBATCH --time=00:10:00
#SBATCH --mem-per-cpu=100
#SBATCH --nodes=1


MATDIR=$(readlink $1 -f)
OUTDIR=$(readlink $2 -f)

# Run sbatch and parse for job id
sjob()
{
 JID=$(sbatch "$@" | grep -Po "(\d+)")
 echo $JID
}

# Run a dependancy pipeline of slurm jobs, each argument is a command that should be run
spipe()
{
 JID=$SLURM_JOB_ID

 for SJOB in "$@"
 do
   JID=$(sjob --dependency=afterany:$JID $SJOB)
   echo $JID
 done
}

spipe "./ftleVidFrame.sh $MATDIR $OUTDIR" \
"./ftleCombine.sh $OUTDIR" 

# TODO: Clean
