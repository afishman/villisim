#!/bin/bash
#
#SBATCH --job-name=ftlevidframe
#SBATCH --output=ftlevidframeout
#
#SBATCH --ntasks=1
#SBATCH --time=00:40:00
#SBATCH --mem-per-cpu=20000
#SBATCH --nodes=1
#
#SBATCH --array=1-1000%15

MATDIR=$(readlink $1 -f)
OUTDIR=$(readlink $2 -f)

mkdir -p $OUTDIR

cd /home/afishman/flow-modelling/afishman/streaklines/

echo ftlevid $SLURM_ARRAY_TASK_ID

module load matlab/2017b
srun sh mrun.sh ftleVidFrame $MATDIR $OUTDIR $SLURM_ARRAY_TASK_ID

echo done
