##Set base resolution: number of cells in 1 xboxsize
nx = 256;

##Mid-section
nxbox = 6*nx+1;
nybox = nx+1;
xboxsize = 6.0;
yboxsize = 2.0;
xbox = linspace(-xboxsize,xboxsize,nxbox);
ybox = linspace(0,yboxsize,nybox);

##Stretch downstream
nxdown =1.0*nx+1;
xdown = xboxsize + linstretch((xbox(2)-xbox(1)),nxdown,8.0);

##Stretch upstream
nxup = 1.0*nx+1;
xup = flipud(-(xboxsize + linstretch((xbox(2)-xbox(1)),nxup,8.0)));

##Stretch laterally
nypos = 3.0*nx+1;
ypos = yboxsize + linstretch((ybox(2)-ybox(1)),nypos,8.0);

##Write out to file
fid = fopen("nodestemp.dat","w");
fprintf(fid,"%d %d 1\n", nxbox+nxdown+nxup-2,nybox+nypos-1);
#fprintf(fid,"%d %d 1\n", nxbox,nybox+nypos-1);

##x-locations
for n=(1:nxup-1)
  fprintf(fid,"%20.12f\n", xup(n));
endfor
for n=(1:nxbox)
  fprintf(fid,"%20.12f\n", xbox(n));
endfor
for n=(2:nxdown)
  fprintf(fid,"%20.12f\n", xdown(n));
endfor

#y-locations
for n=(1:nybox)
  fprintf(fid,"%20.12f\n", ybox(n));
endfor
for n=(2:nypos)
  fprintf(fid,"%20.12f\n", ypos(n));
endfor

fclose(fid);
