close all

alpha = 0.70;
P = 1;

num_sections = 11;

num_iterations = 20;

N = zeros(num_iterations, num_sections);
N(1,round(num_sections/2)) = 375;

for i=2:num_iterations
    for j=2:size(N,2)-1
       N(i, j) = (alpha/2)*(N(i-1, j-1) + N(i-1, j+1)) + (1-alpha)*N(i-1,j);
    end
end

hold on; grid on
for i=[1,5,10,15,20]
    plot(N(i,:))
end