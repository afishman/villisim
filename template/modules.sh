# Barry
module load imkl/2018.1.163 icc/2018.1.163-gcc-6.4.0 openmpi/3.0.0

# Python
module load python/2.7.14 ffmpeg/3.4.2 
module load numpy/1.14.1-python-2.7.14 scipy/1.0.0-python-2.7.14 
module load matplotlib/2.2.2-python-2.7.14 pandas/0.22.0-python-2.7.14

