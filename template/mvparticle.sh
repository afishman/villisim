#!/bin/bash

## Script to mv particle location files to a new file to keep a time
## history

NFILES=`ls -1 particles.dat* | wc -l | cut -d " " -f 1`
mv particles.dat particles.dat_$NFILES
