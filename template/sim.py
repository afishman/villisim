import json
import numpy as np
import sys

run_file = "./runfile"
default_sim = "./sim.json"

class Sim:
    def __init__(self,
        # Main Controls
        name,
        step_size,
        steps_per_oscillation,
        num_oscillations,
        tecp_rate_in_step_size_units,
        num_bodies,
        tecplot,
        tolerance,
        delta,
        dx,
        frequency,
        amplitude,
        center,
        pattern,
        re
    ):

        self.run_file = run_file
        self.name = name
        self.step_size = step_size
        self.steps_per_oscillation = steps_per_oscillation
        self.num_oscillations = num_oscillations
        self.tecp_rate_in_step_size_unit=tecp_rate_in_step_size_units
        self.num_bodies = num_bodies
        self.tecplot = tecplot
        self.tolerance = tolerance
        self.delta = delta
        self.dx = dx
        self.frequency = float(frequency)
        self.amplitude = float(amplitude)
        self.center = center
        self.pattern = pattern
        self.re = int(re)

    def oscillate(self, i):
        # TODO: make it classy
        if self.pattern=="inphase":
            return self.frequency, self.amplitude, self.center

        elif self.pattern=="antiphase":
            center = 0 if i%2 else np.pi
            return self.frequency, self.amplitude, center

        else:
            raise Exception("Unknown pattern type: " + self.pattern)


def load(filepath=default_sim):
    with open(filepath, 'r') as f:
        data = json.load(f)

    return Sim(**data)

# Get/set a sim parameter
def main(args):
    sim = load()
    filepath = default_sim

    print(args)

    var = args[1]

    if args[0] == "get":
        print(sim.__dict__[var])

    elif args[0] == "set":
        val = args[2]

        with open(filepath, 'r') as f:
            data = json.load(f)

        if var not in data:
            raise Exception(var + "not found")

        data[var] = val

        with open(filepath, 'w') as f:
            f.write(json.dumps(data, indent=4))

if __name__ == "__main__":
    main(sys.argv[1:])
