#!/bin/bash
## First argument is path to runfile
## Number of processors
#SBATCH --ntasks=16
## Walltime
#SBATCH -t 00-12:00:00
## Total memory
#SBATCH --mem=80000
## Name
#SBATCH -J barryout
## Set the working directory
#SBATCH -D .
#SBATCH --output=barryout
#SBATCH --array=1-1%1

SIMNAME="test_"$SLURM_ARRAY_TASK_ID
echo ****$SIMNAME****

PARAM="re"
declare -a VALS=("1" "10")

RUNFILE='./runfile'
SIMROOT='/home/afishman/flow-modelling/afishman/sims/'
STREAKLINES='/home/afishman/flow-modelling/afishman/streaklines/'

MATDIR="${SIMROOT}mat/${SIMNAME}/"
POSTPROCESSINGDIR="${SIMROOT}plots/${SIMNAME}/"
TEMPLATEDIR="${SIMROOT}templates/${SIMNAME}/"

BARRYPATH=/home/afishman/flow-modelling/afishman/immersed_boundary/branches/parallel/
BARRY=${BARRYPATH}Barry.x

# Run sbatch and parse for job id
sjob()
{
 JID=$(sbatch "$@" | grep -Po "(\d+)")
 echo $JID
}

# Run a dependancy pipeline of slurm jobs, each argument is a command that should be run
spipe()
{
 JID=$SLURM_JOB_ID

 for SJOB in "$@"
 do
   JID=$(sjob --dependency=afterany:$JID $SJOB)
   echo $JID
 done
}

## Load module
echo loading modules
source ~/logon

# Copy template
mkdir -p ${TEMPLATEDIR}
cp -r ./template/ ${TEMPLATEDIR}
#cd ${TEMPLATEDIR}template/

# Set param for batching
val=${VALS[$SLURM_ARRAY_TASK_ID]}
echo val $val
python ./sim.py set name ${SIMNAME}
python ./sim.py set $PARAM ${val}

# Make Run
echo making run...
python make.py $RUNFILE
 
num_steps=$(python num_steps.py)

## Submit postprocessing jobs 
echo submitting jobs

spipe "./copysettings ./ $MATDIR" \
"./copysettings ./ $POSTPROCESSINGDIR" \
"--array=1-${num_steps}%100 ./sdat2mat ./ $MATDIR" \
"./postprocess $MATDIR $POSTPROCESSINGDIR" \
"./clean"

module load imkl/2018.1.163 icc/2018.1.163-gcc-6.4.0 openmpi/3.0.0

echo "Running barry " $SLURM_ARRAY_TASK_ID
cat ./runfile
#srun $BARRY < ./runfile > ./barryout
sh ./sbarry < ./runfile

echo Barry Done
