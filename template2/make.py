"""
    Assembles a simulation from json
"""

import numpy as np
from sim import load
import sys
import shutil


sim_file = "./sim.json"
sim = load()

# Main Controls
# Commands
def write(f, msg):
    if isinstance(msg, list):
        msg = " ".join([str(x) for x in msg])

    f.write(str(msg) + "\n")

def add_body(f, i, x, rotation=0, offset=0.6, center=0.2):
    write(f, "body")
    write(f, 6)
    write(f, "plate.msh")

    write(f, "movb")
    write(f, [i, 0, 0, 90+rotation])

    write(f, "movb")
    write(f, [i, x, offset, 0])

    write(f, "utbc")
    write(f, [i, x, center, 0])

    write(f, "rfor")
    write(f, [i, 10])

    write(f, "")

def oscillate_body(f, i, frequency, amplitude, center):
    write(f, "oscb")
    write(f, [i, frequency, amplitude, center])
    write(f, "move")
    write(f, [i, 7])
    write(f, "")

# Commands
def step(f, n, tecplot=False):
    # Save output of previous state
    write(f, "olpl")
    write(f, "shel")

    if tecplot:
        write(f, "tecp")

    # Take a step
    write(f, "step")
    write(f, n)
    write(f, "")

def set_tolerance(f, tolerance, delta):
    write(f, "tole")
    write(f, tolerance)
    write(f, 8)
    write(f, "delt")
    write(f, delta)
    write(f, "")

def init_bodies(f):
    write(f, "inib")
    write(f, "")

def init_particles(f):
    write(f, "part")
    write(f, "")    

def num_steps(sim):
    return 1+int(sim.steps_per_oscillation*sim.num_oscillations/sim.step_size)

def main(args):
    # Set RE
    with open("./parameters.dat", 'r') as f:
        lines = f.readlines()

    print(lines)

    lines[0] = "%i 2\n" % sim.re 

    with open ("./parameters.dat", 'w') as f:
        f.writelines(lines)

    return

    # Write output
    with open(sim.run_file, "w") as f:
        init_particles(f)

        # Add Bodies left to right centered in the middle of the domain
        width = sim.dx*(sim.num_bodies-1)
        X = np.linspace(-width/2.0, width/2.0, sim.num_bodies)
        for i, x in enumerate(X):
            add_body(f, i+1, x)

        init_bodies(f)

        # Set oscillations
        for i in range(sim.num_bodies):
            frequency, amplitude, center = sim.oscillate(i)
            oscillate_body(f, i+1, frequency, amplitude, center)

        set_tolerance(f, sim.tolerance, sim.delta)

        # Steppin'
        for i in range(num_steps(sim)):
            step(f, sim.step_size, sim.tecplot)
        
        write(f, "exit")
        print("made sim")

if __name__ == "__main__":
    main(sys.argv[1:])
