base_path = './rpafBase'
step_path = './rpafStep'
out_file = './runfile'
num_steps = 65*10


with open(base_path, 'r') as file:
    base = file.read()


with open(step_path, 'r') as file:
    step = file.read()

with open(out_file, 'w') as file:
    file.write(base)

    for i in range(num_steps):
        file.write(step)

    file.write("exit\n")


